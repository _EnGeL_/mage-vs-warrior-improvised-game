package characters;

/**
 * Created by Артем on 15.12.2016.
 */

public class Mage {

    public int hp;
    public int mp;
    public boolean isInvulnerable;
    public boolean canUseAir;

    public Mage () {
        hp = 50;
        mp = 0;
        isInvulnerable = false;
        canUseAir = true;
    }

    public int useFireball (int hp) {     //3 mp
        hp-=3;
        return hp;
    }

    public int useFrostbolt (int hp) {      //3 mp
        hp-=1;
        return hp;
    }

    public boolean useFrostbolt_1 (boolean isPossibleToUseWeapon) {
        isPossibleToUseWeapon = false;
        return isPossibleToUseWeapon;
    }

    public int useBoulder (int armor) {      //4 mp
        armor-=5;
        return armor;
    }

    public int useAir (int hp) {      //3 mp
        hp+=5;
        return hp;
    }

    public int getFlameForm (int hp) {       //6 mp
        hp-=10;
        return hp;
    }

    public int getWaterForm (int hp) {       //6 mp
        hp-=7;
        return hp;
    }

    public boolean getWaterForm_1 (boolean isPossibleToUseWeapon) {
        isPossibleToUseWeapon = false;
        return isPossibleToUseWeapon;
    }

    public int getEarthForm (int armor) {      //6 mp
        armor -= armor;
        return armor;
    }

    public int getAirForm (int hp) {      //6 mp
        hp+=6;
        return hp;
    }

    public boolean getAirForm_1 (boolean isInvulnerable) {
        isInvulnerable = true;
        return isInvulnerable;
    }

}
