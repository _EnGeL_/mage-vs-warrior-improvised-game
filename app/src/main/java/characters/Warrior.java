package characters;

/**
 * Created by Артем on 15.12.2016.
 */

public class Warrior {

    public int hp;
    public int mp;
    public int armor;
    public boolean canUseWeapon;

    public Warrior () {
        hp = 50;
        mp = 0;
        armor = 0;
        canUseWeapon = true;
    }

    public int useWeapon (int hp) {      //3 mp
        hp-=7;
        return hp;
    }

    public int useWeapon_1 (int hp) {
        hp-=3;
        return hp;
    }

    public boolean removeInvulnerable (boolean isInvulnerable) {       //3 mp
        isInvulnerable = false;
        return isInvulnerable;
    }

    public int useCrush (int hp) {       //4 mp
        hp-=2;
        return hp;
    }

    public boolean useCrush_1 (boolean canUseAir) {
        canUseAir = false;
        return canUseAir;
    }

    public int armorUp (int armor) {       //3 mp
        armor+=4;
        return armor;
    }

    public int useWeaponRage (int hp) {      //6 mp
        hp-=15;
        return hp;
    }

    public int useWeaponRage_1 (int hp) {
        hp-=3;
        return hp;
    }

    public boolean removeInvulnerableRage (boolean isInvulnerable) {      //6 mp
        isInvulnerable = false;
        return isInvulnerable;
    }

    public int removeInvulnerableRage_1 (int hp) {
        hp-=5;
        return hp;
    }

    public int useCrushRage (int hp) {      //6 mp
        hp-=6;
        return hp;
    }

    public boolean useCrushRage_1 (boolean canUseAir) {
        canUseAir = false;
        return canUseAir;
    }

    public int armorUpRage (int armor) {      //6 mp
        armor+=12;
        return armor;
    }

}
