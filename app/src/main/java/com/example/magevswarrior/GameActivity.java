package com.example.magevswarrior;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import characters.Mage;
import characters.Warrior;
import writers.HerosImageInstaller;
import writers.ResAndBuffsWriter;

import static com.example.magevswarrior.R.id.end_turn;
import static com.example.magevswarrior.R.id.spell_1;
import static com.example.magevswarrior.R.id.spell_2;
import static com.example.magevswarrior.R.id.spell_3;
import static com.example.magevswarrior.R.id.spell_4;
import static com.example.magevswarrior.R.id.spell_5;
import static com.example.magevswarrior.R.id.spell_6;
import static com.example.magevswarrior.R.id.spell_7;
import static com.example.magevswarrior.R.id.spell_8;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    Intent End;
    LinearLayout ChangeTurn;
    TextView Hero1Res, Hero2Res, Hero1Buffs, Hero2Buffs;
    ImageView Hero1, Hero2, Spell1, Spell2, Spell3, Spell4, Spell5, Spell6, Spell7, Spell8, EndTurn;
    int isMage;
    static int whoWin;
    Mage mage;
    Warrior war;
    Toast mageSpell1, mageSpell2, mageSpell3, mageSpell4, mageSpell5, mageSpell6, mageSpell7, mageSpell8, warSpell1, warSpell2, warSpell3, warSpell4, warSpell5, warSpell6, warSpell7, warSpell8, itsNotPossible, endGame;
    ResAndBuffsWriter RBW = new ResAndBuffsWriter();
    HerosImageInstaller HII = new HerosImageInstaller();

    @Override
    protected void onCreate(Bundle game) {
        super.onCreate(game);
        setContentView(R.layout.activity_game);

        /*setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);*/

        End = new Intent(this, EndGameActivity.class);

        mageSpell1 = Toast.makeText(getApplicationContext(), "Вы использовали Огненный шар!", Toast.LENGTH_SHORT);
        mageSpell2 = Toast.makeText(getApplicationContext(), "Вы использовали Ледяной Шар!", Toast.LENGTH_SHORT);
        mageSpell3 = Toast.makeText(getApplicationContext(), "Вы использовали Силу Земли!", Toast.LENGTH_SHORT);
        mageSpell4 = Toast.makeText(getApplicationContext(), "Вы полечили себя!", Toast.LENGTH_SHORT);
        mageSpell5 = Toast.makeText(getApplicationContext(), "Вы использовали улучшенный Огненный Шар", Toast.LENGTH_SHORT);
        mageSpell6 = Toast.makeText(getApplicationContext(), "Вы использовали улучшенный Ледяной Шар!", Toast.LENGTH_SHORT);
        mageSpell7 = Toast.makeText(getApplicationContext(), "Вы использовали Натурализацию!", Toast.LENGTH_SHORT);
        mageSpell8 = Toast.makeText(getApplicationContext(), "Вы получили Силу Ветра!", Toast.LENGTH_SHORT);
        warSpell1 = Toast.makeText(getApplicationContext(), "Вы использовали Оружие!", Toast.LENGTH_SHORT);
        warSpell2 = Toast.makeText(getApplicationContext(), "Вы убрали Неуязвимость!", Toast.LENGTH_SHORT);
        warSpell3 = Toast.makeText(getApplicationContext(), "Вы ослабили Хэлада!", Toast.LENGTH_SHORT);
        warSpell4 = Toast.makeText(getApplicationContext(), "Вы укрепили броню!", Toast.LENGTH_SHORT);
        warSpell5 = Toast.makeText(getApplicationContext(), "Вы использовали улучшенное Оружие!", Toast.LENGTH_SHORT);
        warSpell6 = Toast.makeText(getApplicationContext(), "Вы нанесли рассекающий удар!", Toast.LENGTH_SHORT);
        warSpell7 = Toast.makeText(getApplicationContext(), "Вы сокрушили Хэлада!", Toast.LENGTH_SHORT);
        warSpell8 = Toast.makeText(getApplicationContext(), "Вы одели улучшенную броню!", Toast.LENGTH_SHORT);
        itsNotPossible = Toast.makeText(getApplicationContext(), "Нельзя это сделать", Toast.LENGTH_SHORT);
        endGame = Toast.makeText(getApplicationContext(), "Игра закончена!", Toast.LENGTH_SHORT);

        mage = new Mage();
        mage.mp += (int)(Math.random()*3+2);

        war = new Warrior();

        isMage = 1;

        Hero1Res = (TextView) findViewById(R.id.hero_1_hp_and_mp);
        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
        Hero1Buffs = (TextView) findViewById(R.id.hero_1_buffs);
        RBW.MageBuffsWriter(Hero1Buffs, mage.isInvulnerable, mage.canUseAir);

        Hero2Res = (TextView) findViewById(R.id.hero_2_hp_and_mp);
        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
        Hero2Buffs = (TextView) findViewById(R.id.hero_2_buffs);
        RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);

        ChangeTurn = (LinearLayout) findViewById(R.id.activity_game);

        Spell1 = (ImageView) findViewById(R.id.spell_1);
        Spell1.setImageResource(R.drawable.fireball);
        Spell1.setOnClickListener(this);
        Spell2 = (ImageView) findViewById(R.id.spell_2);
        Spell2.setImageResource(R.drawable.frostbolt);
        Spell2.setOnClickListener(this);
        Spell3 = (ImageView) findViewById(R.id.spell_3);
        Spell3.setImageResource(R.drawable.boulder);
        Spell3.setOnClickListener(this);
        Spell4 = (ImageView) findViewById(R.id.spell_4);
        Spell4.setImageResource(R.drawable.air);
        Spell4.setOnClickListener(this);
        Spell5 = (ImageView) findViewById(R.id.spell_5);
        Spell5.setImageResource(R.drawable.upfireball);
        Spell5.setOnClickListener(this);
        Spell6 = (ImageView) findViewById(R.id.spell_6);
        Spell6.setImageResource(R.drawable.upfrostbolt);
        Spell6.setOnClickListener(this);
        Spell7 = (ImageView) findViewById(R.id.spell_7);
        Spell7.setImageResource(R.drawable.upboulder);
        Spell7.setOnClickListener(this);
        Spell8 = (ImageView) findViewById(R.id.spell_8);
        Spell8.setImageResource(R.drawable.upair);
        Spell8.setOnClickListener(this);

        Hero1 = (ImageView) findViewById(R.id.hero_1);
        HII.magesImageInstaller(Hero1, mage.isInvulnerable, mage.canUseAir);
        Hero2 = (ImageView) findViewById(R.id.hero_2);
        HII.warsImageInstaller(Hero2, war.canUseWeapon);
        EndTurn = (ImageView) findViewById(R.id.end_turn);
        EndTurn.setImageResource(R.drawable.end_turn_pic);
        EndTurn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case end_turn:      //Смена хода
                    if (isMage == 1) {     //Если ход мага
                        ChangeTurn.setBackgroundResource(R.drawable.war_turn_back);
                        Spell1.setImageResource(R.drawable.weapon);
                        Spell2.setImageResource(R.drawable.remove);
                        Spell3.setImageResource(R.drawable.crush);
                        Spell4.setImageResource(R.drawable.armor);
                        Spell5.setImageResource(R.drawable.upweapon);
                        Spell6.setImageResource(R.drawable.upremove);
                        Spell7.setImageResource(R.drawable.upcrush);
                        Spell8.setImageResource(R.drawable.uparmor);
                        HII.warsImageInstaller(Hero1, war.canUseWeapon);
                        HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                        war.mp += (int) (Math.random() * 3 + 2);
                        if (!mage.canUseAir) {
                            mage.canUseAir = true;
                        }
                        RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                        RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                        RBW.WarriorBuffsWriter(Hero1Buffs, war.canUseWeapon);
                        RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                        isMage = 2;
                    } else {    // если ход воина
                        ChangeTurn.setBackgroundResource(R.drawable.mage_turn_back);
                        Spell1.setImageResource(R.drawable.fireball);
                        Spell2.setImageResource(R.drawable.frostbolt);
                        Spell3.setImageResource(R.drawable.boulder);
                        Spell4.setImageResource(R.drawable.air);
                        Spell5.setImageResource(R.drawable.upfireball);
                        Spell6.setImageResource(R.drawable.upfrostbolt);
                        Spell7.setImageResource(R.drawable.upboulder);
                        Spell8.setImageResource(R.drawable.upair);
                        HII.magesImageInstaller(Hero1, mage.isInvulnerable, mage.canUseAir);
                        HII.warsImageInstaller(Hero2, war.canUseWeapon);
                        mage.mp += (int) (Math.random() * 3 + 2);
                        if (!war.canUseWeapon) {
                            war.canUseWeapon = true;
                        }
                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                        RBW.MageBuffsWriter(Hero1Buffs, mage.isInvulnerable, mage.canUseAir);
                        RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                        isMage = 1;
                    }
                    break;

                case spell_1:    //первая способность
                    if (isMage == 1) {      //механика огненного шара
                        if (mage.mp >= 3) {
                            mage.mp -= 3;
                            mageSpell1.show();
                            if (war.armor > 0) {    //если у воина есть броня
                                war.armor = mage.useFireball(war.armor);
                                switch (war.armor) {

                                    case -1:
                                        war.armor = 0;
                                        war.hp -= 1;
                                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                        if (war.hp <= 0) {
                                            endGame.show();
                                            whoWin = 0;
                                            startActivity(End);
                                            this.finish();
                                        }
                                        break;

                                    case -2:
                                        war.armor = 0;
                                        war.hp -= 2;
                                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                        if (war.hp <= 0) {
                                            endGame.show();
                                            whoWin = 0;
                                            startActivity(End);
                                            this.finish();
                                        }
                                        break;

                                    default:
                                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                        break;
                                }
                            } else {    //если брони нет
                                war.hp = mage.useFireball(war.hp);
                                RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                if (war.hp <= 0) {
                                    endGame.show();
                                    whoWin = 0;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {    //невозможность совершить действие
                            itsNotPossible.show();
                        }
                    } else {     //механика оружия
                        if (war.mp >= 3 && war.canUseWeapon) {
                            war.mp -= 3;
                            warSpell1.show();
                            if (mage.isInvulnerable) {    //если маг неуязвим
                                mage.isInvulnerable = false;
                                war.hp = war.useWeapon_1(war.hp);
                                if (war.hp <= 0) {
                                    war.hp = 1;
                                }
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                            } else {     //если маг уязвим
                                mage.hp = war.useWeapon(mage.hp);
                                war.hp = war.useWeapon_1(war.hp);
                                if (war.hp <= 0) {
                                    war.hp = 1;
                                }
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                                if (mage.hp <= 0) {
                                    endGame.show();
                                    whoWin = 1;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {
                            itsNotPossible.show();     //невозможность совершить действие
                        }
                    }
                    break;

                case spell_2:      //вторая способность
                    if (isMage == 1) {       //механика ледяного шара
                        if (mage.mp >= 3) {
                            mage.mp -= 3;
                            mageSpell2.show();
                            war.canUseWeapon = mage.useFrostbolt_1(war.canUseWeapon);
                            if (war.armor > 0) {     //если у воина есть броня
                                war.armor = mage.useFrostbolt(war.armor);
                                RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                                HII.warsImageInstaller(Hero2, war.canUseWeapon);
                            } else {      //если брони нет
                                war.hp = mage.useFrostbolt(war.hp);
                                RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                                HII.warsImageInstaller(Hero2, war.canUseWeapon);
                                if (war.hp <= 0) {
                                    endGame.show();
                                    whoWin = 0;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {
                            itsNotPossible.show();     //невозможность совершить действие
                        }
                    } else {     //механика ремува неуязвимости
                        if (war.mp >= 3 && mage.isInvulnerable) {
                            warSpell2.show();
                            mage.isInvulnerable = war.removeInvulnerable(mage.isInvulnerable);
                            RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                            RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                            HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                        } else {
                            itsNotPossible.show();    //невозможность совершить действие
                        }
                    }
                    break;

                case spell_3:     //третья способность
                    if (isMage == 1) {     //механика работы каменной глыбы
                        if (mage.mp >= 4 && war.armor > 0) {
                            mageSpell3.show();
                            mage.mp -= 4;
                            war.armor = mage.useBoulder(war.armor);
                            if (war.armor < 0) {
                                war.armor = 0;
                            }
                            RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                            RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                        } else {
                            itsNotPossible.show();      //невозможность совершить действие
                        }
                    } else {      //механика работы слабости
                        if (war.mp >= 4) {
                            warSpell3.show();
                            war.mp -= 4;
                            mage.canUseAir = war.useCrush_1(mage.canUseAir);
                            if (mage.isInvulnerable) {      //если маг неуязвим
                                mage.isInvulnerable = false;
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                            } else {    //если маг уязвим
                                mage.hp = war.useCrush(mage.hp);
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                                if (mage.hp <= 0) {
                                    endGame.show();
                                    whoWin = 1;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {
                            itsNotPossible.show();     //невозможность совершить действие
                        }
                    }
                    break;

                case spell_4:     //четвертая способность
                    if (isMage == 1) {      //механика работы хила
                        if (mage.mp >= 3 && mage.hp < 50 && mage.canUseAir) {
                            mageSpell4.show();
                            mage.mp -= 3;
                            mage.hp = mage.useAir(mage.hp);
                            if (mage.hp > 50) {
                                mage.hp = 50;
                            }
                            RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                        } else {
                            itsNotPossible.show();     //невозможность совершить действие
                        }
                    } else {     //механика работы брони
                        if (war.mp >= 3) {
                            warSpell4.show();
                            war.mp -= 3;
                            war.armor = war.armorUp(war.armor);
                            RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                        } else {
                            itsNotPossible.show();     //невозможность совершить действие
                        }
                    }
                    break;

                case spell_5:
                    if (isMage == 1) {
                        if (mage.mp >= 6) {
                            mageSpell5.show();
                            mage.mp -= 6;
                            if (war.armor > 0) {
                                war.armor = mage.getFlameForm(war.armor);
                                if (war.armor < 0) {
                                    war.hp += war.armor;
                                    war.armor = 0;
                                    RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                    RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                    if (war.hp <= 0) {
                                        endGame.show();
                                        whoWin = 0;
                                        startActivity(End);
                                        this.finish();
                                    }
                                } else {
                                    RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                    RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                }
                            } else {
                                war.hp = mage.getFlameForm(war.hp);
                                RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                if (war.hp <= 0) {
                                    endGame.show();
                                    whoWin = 0;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {
                            itsNotPossible.show();
                        }
                    } else {
                        if (war.mp >= 6 && war.canUseWeapon) {
                            warSpell5.show();
                            war.mp -= 6;
                            if (mage.isInvulnerable) {
                                mage.isInvulnerable = false;
                                war.hp = war.useWeaponRage_1(war.hp);
                                if (war.hp <= 0) {
                                    war.hp = 1;
                                }
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                            } else {
                                mage.hp = war.useWeaponRage(mage.hp);
                                war.hp = war.useWeaponRage_1(war.hp);
                                if (mage.hp <= 0) {
                                    endGame.show();
                                    whoWin = 1;
                                    startActivity(End);
                                    this.finish();
                                }
                                if (war.hp <= 0) {
                                    war.hp = 1;
                                }
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                            }
                        } else {
                            itsNotPossible.show();
                        }
                    }
                    break;

                case spell_6:
                    if (isMage == 1) {
                        if (mage.mp >= 6) {
                            mageSpell6.show();
                            mage.mp -= 6;
                            war.canUseWeapon = mage.getWaterForm_1(war.canUseWeapon);
                            switch (war.armor) {
                                case 0:
                                    war.hp = mage.getWaterForm(war.hp);
                                    RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                    RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                    RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                                    HII.warsImageInstaller(Hero2, war.canUseWeapon);
                                    if (war.hp <= 0) {
                                        endGame.show();
                                        whoWin = 0;
                                        startActivity(End);
                                        this.finish();
                                    }
                                break;
                                default:
                                    war.armor = mage.getWaterForm(war.armor);
                                    if (war.armor < 0) {
                                        war.hp += war.armor;
                                        war.armor = 0;
                                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                        RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                                        HII.warsImageInstaller(Hero2, war.canUseWeapon);
                                        if (war.hp <= 0) {
                                            endGame.show();
                                            whoWin = 0;
                                            startActivity(End);
                                            this.finish();
                                        }
                                    } else {
                                        RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                                        RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                                        RBW.WarriorBuffsWriter(Hero2Buffs, war.canUseWeapon);
                                        HII.warsImageInstaller(Hero2, war.canUseWeapon);
                                    }
                                break;
                            }
                        } else {
                            itsNotPossible.show();
                        }
                    } else {
                        if (war.mp >= 6 && mage.isInvulnerable) {
                            warSpell6.show();
                            war.mp -= 6;
                            mage.isInvulnerable = war.removeInvulnerableRage(mage.isInvulnerable);
                            mage.hp = war.removeInvulnerableRage_1(mage.hp);
                            RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                            RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                            RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                            HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                            if (mage.hp <= 0) {
                                endGame.show();
                                whoWin = 1;
                                startActivity(End);
                                this.finish();
                            }
                        } else {
                            itsNotPossible.show();
                        }
                    }
                    break;

                case spell_7:
                    if (isMage == 1) {
                        if (mage.mp >= 6 && war.armor > 0) {
                            mageSpell7.show();
                            mage.mp -= 6;
                            war.armor = mage.getEarthForm(war.armor);
                            RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                            RBW.WarriorResWriter(Hero2Res, war.hp, war.mp, war.armor);
                        } else {
                            itsNotPossible.show();
                        }
                    } else {
                        if (war.mp >= 6) {
                            warSpell7.show();
                            war.mp -= 6;
                            mage.canUseAir = war.useCrushRage_1(mage.canUseAir);
                            if (mage.isInvulnerable) {
                                mage.isInvulnerable = false;
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                            } else {
                                mage.hp = war.useCrushRage(mage.hp);
                                RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                                RBW.MageResWriter(Hero2Res, mage.hp, mage.mp);
                                RBW.MageBuffsWriter(Hero2Buffs, mage.isInvulnerable, mage.canUseAir);
                                HII.magesImageInstaller(Hero2, mage.isInvulnerable, mage.canUseAir);
                                if (mage.hp <= 0) {
                                    endGame.show();
                                    whoWin = 1;
                                    startActivity(End);
                                    this.finish();
                                }
                            }
                        } else {
                            itsNotPossible.show();
                        }
                    }
                    break;

                case spell_8:
                    if (isMage == 1) {
                        if (mage.mp >= 6 && mage.canUseAir) {
                            mageSpell8.show();
                            mage.mp -= 6;
                            mage.isInvulnerable = mage.getAirForm_1(mage.isInvulnerable);
                            mage.hp = mage.getAirForm(mage.hp);
                            if (mage.hp > 50) {
                                mage.hp = 50;
                            }
                            RBW.MageResWriter(Hero1Res, mage.hp, mage.mp);
                            RBW.MageBuffsWriter(Hero1Buffs, mage.isInvulnerable, mage.canUseAir);
                            HII.magesImageInstaller(Hero1, mage.isInvulnerable, mage.canUseAir);
                        } else {
                            itsNotPossible.show();
                        }
                    } else {
                        if (war.mp >= 6) {
                            warSpell8.show();
                            war.mp -= 6;
                            war.armor = war.armorUpRage(war.armor);
                            RBW.WarriorResWriter(Hero1Res, war.hp, war.mp, war.armor);
                        } else {
                            itsNotPossible.show();
                        }
                    }
                    break;
            }
    }
}
