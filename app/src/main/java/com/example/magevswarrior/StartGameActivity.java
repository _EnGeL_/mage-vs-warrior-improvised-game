package com.example.magevswarrior;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class StartGameActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView MageHistory, WarriorHistory, StartButton;

    @Override
    protected void onCreate(Bundle startGame) {
        super.onCreate(startGame);
        setContentView(R.layout.activity_start);
        /*setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);*/

        MageHistory = (ImageView) findViewById(R.id.mage_history);
        WarriorHistory = (ImageView) findViewById(R.id.war_history);
        StartButton = (ImageView) findViewById(R.id.start);
        StartButton.setOnClickListener(this);

        MageHistory.setImageResource(R.drawable.mage_history);
        WarriorHistory.setImageResource(R.drawable.war_history);
        StartButton.setImageResource(R.drawable.start_button);
    }

    @Override
    public void onClick(View view) {
        Toast toast = Toast.makeText(getApplicationContext(),"Определилсь, кто за кого? Отлично!", Toast.LENGTH_LONG);
        toast.show();
        Intent start = new Intent(this, GameActivity.class);
        startActivity(start);
    }
}
