package com.example.magevswarrior;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static com.example.magevswarrior.GameActivity.whoWin;

public class EndGameActivity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout Winner;
    ImageView Restart;
    Intent restartGame;

    @Override
    protected void onCreate (Bundle gameEnd) {
        super.onCreate(gameEnd);
        setContentView(R.layout.activity_end);
        /*setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);*/
        Winner = (LinearLayout) findViewById(R.id.activity_end);
        try {
            switch (whoWin) {
                case 0:
                    Winner.setBackgroundResource(R.drawable.magewin);
                break;
                case 1:
                    Winner.setBackgroundResource(R.drawable.warwin);
                break;
            }
        } catch (Exception e) {
            Log.d("ошибка", "ошибка");
        }
        Restart = (ImageView) findViewById(R.id.restart);
        Restart.setImageResource(R.drawable.restart_game);
        Restart.setOnClickListener(this);
        restartGame = new Intent(this, StartGameActivity.class);
    }

    @Override
    public void onClick(View view) {
        startActivity(restartGame);
    }
}
