package com.example.magevswarrior;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView StartGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);*/
        StartGame = (ImageView) findViewById(R.id.startgame);
        StartGame.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Toast toast =  Toast.makeText(getApplicationContext(), "Выберите вашего героя!", Toast.LENGTH_SHORT);
        toast.show();
        Intent startGame = new Intent(this, StartGameActivity.class);
        startActivity(startGame);
    }
}
