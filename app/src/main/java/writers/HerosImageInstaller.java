package writers;

import android.widget.ImageView;

import com.example.magevswarrior.R;

/**
 * Created by Артем on 23.12.2016.
 */

public class HerosImageInstaller {

    public void magesImageInstaller (ImageView magesImage, boolean isInvulnerable, boolean canUseAir) {
        if (isInvulnerable && canUseAir) {
            magesImage.setImageResource(R.drawable.invulnerable_mage);
        } else {
            if (!isInvulnerable && !canUseAir) {
                magesImage.setImageResource(R.drawable.cant_heal_mage);
            } else {
                magesImage.setImageResource(R.drawable.mage);
            }
        }
    }

    public void warsImageInstaller (ImageView warsImage, boolean canUseWeapon) {
        if (canUseWeapon) {
            warsImage.setImageResource(R.drawable.war);
        } else {
            warsImage.setImageResource(R.drawable.frozen_war);
        }
    }

}
