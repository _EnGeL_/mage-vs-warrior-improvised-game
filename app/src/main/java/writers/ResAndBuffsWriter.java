package writers;

import android.widget.TextView;

public class ResAndBuffsWriter {

    public void MageResWriter (TextView Hero1Res, int hp, int mp) {
        Hero1Res.setText(hp + " жизни, " + mp + " маны.");
    }

    public void MageBuffsWriter (TextView Hero1Buffs, boolean isInvulnerable, boolean canUseAir) {
        if (!isInvulnerable && canUseAir) {
            Hero1Buffs.setText("Уязвим и может использовать лечение");
        } else {
            if (isInvulnerable && !canUseAir) {
                Hero1Buffs.setText("Неуязвим и не может использовать лечение");
            } else {
                if (!isInvulnerable && !canUseAir) {
                    Hero1Buffs.setText("Уязвим и не может использовать лечение");
                } else {
                    Hero1Buffs.setText("Неуязвим и может использовать лечение");
                }
            }
        }
    }

    public void WarriorResWriter (TextView Hero2Res, int hp, int mp, int armor) {
        Hero2Res.setText(hp + " жизни, " + mp + " маны, " + armor + " брони.");
    }

    public void WarriorBuffsWriter (TextView Hero2Buffs, boolean canUseWeapon) {
        if (canUseWeapon) {
            Hero2Buffs.setText("Может использовать оружие");
        } else {
            Hero2Buffs.setText("Не может использовать оружие");
        }
    }

}
